<?php

class DiscountServiceTest extends TestCase
{
    protected function getDiscountApiResponse(array $data): DiscountServiceTest
    {
        return $this->json('POST', '/discount', $data);
    }

    protected function getValidApiRequestBody(): array
    {
        return [
            'id' => '3',
            'customer-id' => '3',
            'items' => [
                [
                    'product-id' => 'A101',
                    'quantity' => '2',
                    'unit-price' => '9.75',
                    'total' => '19.50',
                ],
                [
                    'product-id' => 'A102',
                    'quantity' => '20',
                    'unit-price' => '49.50',
                    'total' => '990',
                ],
                [
                    'product-id' => 'B102',
                    'quantity' => '6',
                    'unit-price' => '4.99',
                    'total' => '29.94',
                ],
                [
                    'product-id' => 'B101',
                    'quantity' => '11',
                    'unit-price' => '4.99',
                    'total' => '29.94',
                ],
            ],
            'total' => '1069.38',
        ];
    }

    public function testFailedDiscountApiOnMissingIdTest()
    {
        $data = $this->getValidApiRequestBody();
        unset($data['id']);

        $response = $this->getDiscountApiResponse($data);

        $response->assertResponseStatus(422);
        $response->assertContains('"error":"The id field is required."', $this->response->getContent());
    }

    public function testFailedDiscountApiOnMissingCustomerIdTest()
    {
        $data = $this->getValidApiRequestBody();
        unset($data['customer-id']);

        $response = $this->getDiscountApiResponse($data);

        $response->assertResponseStatus(422);
        $response->assertContains('"error":"The customer-id field is required."', $this->response->getContent());
    }

    public function testFailedDiscountApiOnMissingItemsTest()
    {
        $data = $this->getValidApiRequestBody();
        unset($data['items']);

        $response = $this->getDiscountApiResponse($data);

        $response->assertResponseStatus(422);
        $response->assertContains('"error":"The items field is required."', $this->response->getContent());
    }

    public function testFailedDiscountApiOnMissingTotalTest()
    {
        $data = $this->getValidApiRequestBody();
        unset($data['total']);

        $response = $this->getDiscountApiResponse($data);

        $response->assertResponseStatus(422);
        $response->assertContains('"error":"The total field is required."', $this->response->getContent());
    }

    public function testDiscountApiTypeOneTest()
    {
        $data = $this->getValidApiRequestBody();

        $response = $this->getDiscountApiResponse($data);

        $response->assertResponseStatus(200);
        $response->assertContains('{"type one":106.94}', $this->response->getContent());
    }

    public function testDiscountApiTypeTwoForThreeFreeProductsTest()
    {
        $data = $this->getValidApiRequestBody();

        $response = $this->getDiscountApiResponse($data);

        $response->assertResponseStatus(200);
        $response->assertContains('{"type two":14.97}', $this->response->getContent());
    }

    public function testDiscountApiTypeThreeTest()
    {
        $data = $this->getValidApiRequestBody();

        $response = $this->getDiscountApiResponse($data);

        $response->assertResponseStatus(200);
        $response->assertContains('"type three":1.95}', $this->response->getContent());
    }
}
