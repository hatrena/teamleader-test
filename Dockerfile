FROM semsari/php-container:develop

RUN apt-get -y update && \
    apt-get -y --no-install-recommends install \
    php-soap

ADD . /var/www/html

EXPOSE 80 443

CMD ["/bin/bash", "docker/run.sh"]