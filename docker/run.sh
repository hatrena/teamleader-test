#!/bin/bash

if [ "$APP_DEBUG" = "true" ]; then
    echo "Setting debugging mode...";
    echo "opcache.validate_timestamps=on" >> /etc/php/7.1/mods-available/opcache.ini
    echo "error_reporting = E_ALL" >> /etc/php/7.1/fpm/php.ini
    echo "display_errors = On" >> /etc/php/7.1/fpm/php.ini
    rm /tmp/__CG__*
fi

composer install

echo "Preparing configuration files...";
env | grep APP_ | sed "s/\(.*\)=\(.*\)/env[\1]='\2'/" >> /etc/php/7.1/fpm/pool.d/www.conf
env | grep DB_ | sed "s/\(.*\)=\(.*\)/env[\1]='\2'/" >> /etc/php/7.1/fpm/pool.d/www.conf
env | grep CACHE_ | sed "s/\(.*\)=\(.*\)/env[\1]='\2'/" >> /etc/php/7.1/fpm/pool.d/www.conf

echo "Starting application (php-fpm)...";
/usr/sbin/php-fpm7.1 -t
service php7.1-fpm start

echo "Starting webserver (nginx)...";
nginx -t
nginx -g "daemon off;"
