# Teamleader Test 

### Docker
This test runs on Docker containers. That means that you'll have to install the docker engine (docker ce and docker-compose) https://docs.docker.com/installation/

### Setup Application
Make sure you are in project directory and you have port ```8080``` free, then:

1. ```docker build -t teamleader/test:develop . ```

2. ```docker-compose up```

Now you should be able to open the application on ```http://localhost:8080```

### Api Call
There is one route set for calculating discounts on ```/discount``` via http POST method.

#### Example

Send the following json as body in POST method to ```http://localhost:8080/discount```

```json
{
  "id": "3",
  "customer-id": "3",
  "items": [
    {
      "product-id": "A101",
      "quantity": "2",
      "unit-price": "9.75",
      "total": "19.50"
    },
    {
      "product-id": "A102",
      "quantity": "20",
      "unit-price": "49.50",
      "total": "990"
    },
    {
      "product-id": "B102",
      "quantity": "6",
      "unit-price": "4.99",
      "total": "29.94"
    },
    {
      "product-id": "B101",
      "quantity": "11",
      "unit-price": "4.99",
      "total": "29.94"
    }
  ],
  "total": "1069.38"
}
```

#### Note: 

1. Each method of discount is named as ```type one```, ```type two``` and ```type three```.

2. Each method of discount is calculated separately and overall discount is not accumulated for all types.

### Run tests
To run test, run the command ```docker exec -it teamleader-test vendor/bin/phpunit```