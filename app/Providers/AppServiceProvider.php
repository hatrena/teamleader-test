<?php

namespace App\Providers;

use App\Services\Email\Handlers\EmailHandlerInterface;
use App\Services\Email\Handlers\SendGrid;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmailHandlerInterface::class, SendGrid::class);
    }
}
