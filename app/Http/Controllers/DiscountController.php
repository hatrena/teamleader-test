<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiscountRequest;
use App\Services\DiscountService;
use App\Services\DiscountTypeOneObserver;
use App\Services\DiscountTypeThreeObserver;
use App\Services\DiscountTypeTwoObserver;
use EllipseSynergie\ApiResponse\Contracts\Response;

class DiscountController extends Controller
{
    /*
     * @var Response
     */
    protected $response;

    /*
     * @var DiscountService
     */
    protected $discountService;

    public function __construct(Response $response, DiscountService $discountService)
    {
        $this->response = $response;
        $this->discountService = $discountService;
    }

    public function calculate(DiscountRequest $request)
    {
        $this->discountService
            ->setUp($request)
            ->attach([
                new DiscountTypeOneObserver(),
                new DiscountTypeTwoObserver(),
                new DiscountTypeThreeObserver(),
            ]);


        return $this->response->setStatusCode(200)->withArray([
            'discounts' => $this->discountService->getDiscounts(),
        ]);
    }
}
