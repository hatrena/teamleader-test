<?php

namespace App\Http\Middleware;

use Closure;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /*
     * @var Response
     */
    protected $response;

    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth, Response $response)
    {
        $this->auth = $auth;
        $this->response = $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            return $this->response->errorUnauthorized('Unauthorized Access');
        }

        return $next($request);
    }
}
