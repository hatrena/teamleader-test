<?php

namespace App\Http\Requests;

class DiscountRequest extends ApiRequestValidation
{
    public function rules()
    {
        return [
            'id' => 'required',
            'customer-id' => 'required',
            'items' => 'required|array',
            'items.*.product-id' => 'required',
            'items.*.quantity' => 'required',
            'items.*.unit-price' => 'required',
            'items.*.total' => 'required',
            'total' => 'required',
        ];
    }
}
