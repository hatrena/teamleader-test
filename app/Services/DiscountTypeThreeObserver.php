<?php

namespace App\Services;

use Illuminate\Http\Request;

/*
 * If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.
 */
class DiscountTypeThreeObserver implements ObserverInterface
{
    const DISCOUNT_NAME = 'type three';
    const DISCOUNT_CATEGORY_ID = '1';
    const DISCOUNT_CATEGORY_MIN_COUNT = 2;
    const DISCOUNT_PERCENTAGE = 20;

    public function handle(Request $request): array
    {
        return [
            self::DISCOUNT_NAME => $this->getDiscountedProductAmount($request->items),
        ];
    }

    protected function getDiscountedProductAmount(array $items): float
    {
        $targetProducts = $this->getTargetProducts($items);

        if (count($targetProducts) >= self::DISCOUNT_CATEGORY_MIN_COUNT) {
            $cheapestProductPrice = min(array_column($targetProducts, 'unit-price'));
            return $this->getDiscountPercentage($cheapestProductPrice);
        }

        return 0;
    }

    protected function getTargetProducts(array $items): array
    {
        $products = array_column(config('products'), 'category', 'id');
        $targetProducts = [];
        $discountedProductCounts = 0;

        foreach ($items as $item) {
            if ($products[$item['product-id']] == self::DISCOUNT_CATEGORY_ID) {
                $targetProducts[] = $item;
                $discountedProductCounts += $item['quantity'];
            }
        }

        return $targetProducts;
    }

    protected function getDiscountPercentage(float $value): float
    {
        return number_format(($value * self::DISCOUNT_PERCENTAGE) / 100, 2);
    }
}
