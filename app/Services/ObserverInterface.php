<?php

namespace App\Services;

use Illuminate\Http\Request;

interface ObserverInterface
{
    public function handle(Request $request);
}
