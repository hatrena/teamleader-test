<?php

namespace App\Services;

use Illuminate\Http\Request;

class DiscountService implements SubjectInterface
{
    /*
     * @var array
     */
    protected $observers;

    /*
     * @var Request
     */
    protected $request;

    /*
     * @var array
     */
    protected $discounts;

    public function setUp(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    public function attach($observable)
    {
        if (is_array($observable)) {
            return $this->attachObservable($observable);
        }

        $this->observers[] = $observable;
        return $this;
    }

    public function calculate()
    {
        /* @var $observer ObserverInterface */
        foreach ($this->observers as $observer) {
            $this->discounts[] = $observer->handle($this->request);
        }

        return $this->discounts;
    }

    public function getDiscounts()
    {
        return $this->calculate();
    }

    public function attachObservable($observable)
    {
        foreach ($observable as $observer) {
            if ($observer instanceof ObserverInterface) {
                $this->attach($observer);
            }
        }
    }
}
