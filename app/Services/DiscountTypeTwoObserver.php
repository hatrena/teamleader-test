<?php

namespace App\Services;

use Illuminate\Http\Request;

/*
 * For every product of category "Switches" (id 2), when you buy five, you get a sixth for free.
 */
class DiscountTypeTwoObserver implements ObserverInterface
{
    const DISCOUNT_NAME = 'type two';
    const DISCOUNT_CATEGORY_ID = '2';
    const DISCOUNT_CATEGORY_COUNT = 5;

    public function handle(Request $request): array
    {
        return [
            self::DISCOUNT_NAME => $this->getDiscountedProductAmount($request->items),
        ];
    }

    protected function getDiscountedProductAmount(array $items): float
    {
        $targetProducts = $this->getTargetProducts($items);

        return $this->getTargetProductDiscountAmount($targetProducts);
    }

    protected function getTargetProductDiscountAmount(array $targetProducts): float
    {
        $discount = 0;

        foreach ($targetProducts as $targetProduct) {
            $discountedProductCounts = 0;
            $unitPrice = 0;

            foreach ($targetProduct as $item) {
                $discountedProductCounts += $item['quantity'];
                $unitPrice = $item['unit-price'];
            }

            $allDiscountedProductCounts = floor($discountedProductCounts / self::DISCOUNT_CATEGORY_COUNT);

            $discount += $allDiscountedProductCounts * $unitPrice;
        }

        return $discount;
    }

    protected function getTargetProducts(array $items): array
    {
        $products = array_column(config('products'), 'category', 'id');
        $targetProducts = [];

        foreach ($items as $item) {
            if ($products[$item['product-id']] == self::DISCOUNT_CATEGORY_ID) {
                $targetProducts[$item['product-id']][] = $item;
            }
        }

        return $targetProducts;
    }
}
