<?php

namespace App\Services;

use Illuminate\Http\Request;

/*
 * A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.
 */
class DiscountTypeOneObserver implements ObserverInterface
{
    const DISCOUNT_NAME = 'type one';
    const DISCOUNT_MIN_VALUE = 1000;
    const DISCOUNT_PERCENTAGE_VALUE = 10;

    public function handle(Request $request): array
    {
        return [
            self::DISCOUNT_NAME => $request->total > self::DISCOUNT_MIN_VALUE ? $this->getDiscountPercentage($request->total) : 0,
        ];
    }

    protected function getDiscountPercentage(float $value): float
    {
        return number_format(($value * self::DISCOUNT_PERCENTAGE_VALUE) / 100, 2);
    }
}
