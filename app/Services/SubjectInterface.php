<?php

namespace App\Services;

interface SubjectInterface
{
    public function attach($observable);

    public function calculate();
}
